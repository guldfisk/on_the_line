﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontCollideWith : MonoBehaviour {

    public Transform other;

	// Use this for initialization
	void Awake () {

        foreach (Collider2D myCollider in GetComponentsInChildren<Collider2D>())
        {
            foreach (Collider2D otherCollider in other.GetComponentsInChildren<Collider2D>())
            {
                Physics2D.IgnoreCollision(myCollider, otherCollider);
            }
        }
    }
}
