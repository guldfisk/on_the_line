﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpChargingStation : MonoBehaviour {
    public Bar TargetPlayer;
    private Bar ThisBar;
    private Animator anim;
	// Use this for initialization
	void Start () {
        ThisBar = GetComponent<Bar>();
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.transform.parent && collision.transform.parent.gameObject.GetComponentInChildren<Bar>() && ThisBar.barValue > 0 )
        {
            TargetPlayer = collision.transform.parent.gameObject.GetComponentInChildren<Bar>();
            if(TargetPlayer.barValue <= TargetPlayer.maxValue-1)
            {
                TargetPlayer.barValue += 1;
                ThisBar.barValue -= 1;
            }

            if (ThisBar.barValue == 0)
            {
                anim.enabled = false;
                this.enabled = false;
            }

        }
    }
}
