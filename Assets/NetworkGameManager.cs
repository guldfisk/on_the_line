﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;
using UnityStandardAssets._2D;

public class NetworkGameManager : Photon.PunBehaviour
{

    [Tooltip("The prefab to use for representing the player")]
    public GameObject playerPrefab;

    GameObject localPlayer;

    public override void OnJoinedRoom() {
        if (playerPrefab == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
        }
        else
        {
            Debug.Log("Instantiating LocalPlayer");
            // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
            localPlayer = PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);

            //Follow the player!
            Camera.main.GetComponent<FollowTarget>().target = localPlayer.GetComponentInChildren<PlatformerCharacter2D>().transform;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
