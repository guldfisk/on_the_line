﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Bar))]
public class HP : Photon.PunBehaviour {

    public Transform viking;
    public float decayPerSecond = 2;

    Vector3 positionOffset;
    Bar bar;

	// Use this for initialization
	void Start () {
        positionOffset = transform.position - viking.transform.position;
        bar = GetComponent<Bar>();
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = viking.transform.position + positionOffset;

        bar.barValue -= Time.deltaTime * decayPerSecond;

#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            bar.barValue += bar.maxValue * 0.5f;
        }
#endif

    }
}
