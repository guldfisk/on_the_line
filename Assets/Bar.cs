﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bar : MonoBehaviour {
    public float startRange = 0;
    public float endRange = 1;
    public float startValue = 0;
    public float maxValue = 100;
    public ParticleSystem bloodSystem;
    public Transform filling;
    private float charge = 0;
    public bool playOnAnyChange = false;

    float _barValue = 0;
    public float barValue
    {
        get
        {
            return _barValue;
        }
        set
        {
            charge =  value - _barValue;
            _barValue = Mathf.Clamp(value, 0, maxValue);
            if(bloodSystem && charge >= 1)
            {
                bloodSystem.Emit((int)charge);
                charge = 0;
            }
            else if(playOnAnyChange)
            {
                bloodSystem.Emit((int)1);
            }
                
            float healthPosition = Mathf.Lerp(endRange, startRange, _barValue / maxValue);
            //filling.position.Set(filling.position.x, healthPosition, filling.position.z);
            filling.localPosition = new Vector3(filling.localPosition.x, healthPosition, filling.localPosition.z);
            //Vector3 scale = filling.transform.localScale;
            //scale.x = _barValue / maxValue;
            //filling.transform.localScale = scale;
        }
    }

	// Use this for initialization
	void Awake () {
        //filling = transform.GetChild(0).GetChild(0).GetComponent<Image>();
        barValue = startValue;
    }
	
	// Update is called once per frame
	void Update ()
    {
	}
}
