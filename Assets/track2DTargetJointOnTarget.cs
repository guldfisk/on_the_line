﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class track2DTargetJointOnTarget : MonoBehaviour {
    public GameObject objectToTrack;
    private TargetJoint2D tg2D;
	// Use this for initialization
	void Start () {
        tg2D = GetComponent<TargetJoint2D>();

    }
	
	// Update is called once per frame
	void Update () {
        tg2D.target = objectToTrack.transform.position;

    }
}
