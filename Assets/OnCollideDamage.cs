﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollideDamage : MonoBehaviour {
    private Bar TargetPlayer;
    private Animator anim;
    public float damage = 5;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player") && collision.transform.parent && collision.transform.parent.gameObject.GetComponentInChildren<Bar>())
        {
            TargetPlayer = collision.transform.parent.gameObject.GetComponentInChildren<Bar>();
            if (TargetPlayer.barValue <= TargetPlayer.maxValue - 1)
            {
                TargetPlayer.barValue -= damage;
            }

        }
    }
}
