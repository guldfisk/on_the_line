﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class SpearHit : Photon.MonoBehaviour {

    public Rigidbody2D vikingBody;
    public float thrustCooldown = 0.7f;
    public float thrustSpeed = 5.0f;
    public float thrustRange = 0.7f;

    float ignoreImpulsesBelow = 20;
    float impulseMultiplier = 30;
    float maxImpulseSwinging = 100;
    float maxImpulseThrusting = 100;
    SpringJoint2D joint;
    float lastSpearJumpTime = 0;
    float lastThrustTime = 0;
    bool thrusting = false;
    float thrustOffset = 0.0f;
    Vector2 originalJointAnchor;

    // Use this for initialization
    void Start ()
    {
        Debug.Assert(vikingBody != null, "ERROR: viking not assigned in SpearHit", gameObject);

        joint = GetComponent<SpringJoint2D>();
        originalJointAnchor = joint.anchor;

        if (!photonView.isMine)
            joint.enabled = false; // We'll get position from network instead.
    }
	
	// Update is called once per frame
	void Update () {

        if (!photonView.isMine)
            return; // We'll get position from network instead.

        if (Input.GetButton("Fire1") && (Time.time - lastThrustTime) > thrustCooldown)
        {
            lastThrustTime = Time.time;
            GetComponent<Rigidbody2D>().velocity = transform.right * thrustSpeed * 0.7f;
            thrusting = true;
            thrustOffset = 0.0f;
        }

        if(thrusting)
        {
            thrustOffset += thrustSpeed * Time.deltaTime;
            
            if(thrustSpeed > 0 && thrustOffset >= thrustRange)
            {
                thrustSpeed = -thrustSpeed / 2; //Reverse direction (begin pulling back)
            }
            if (thrustSpeed < 0 && thrustOffset < 0)
            {
                thrusting = false;
                thrustOffset = 0;
                thrustSpeed = -thrustSpeed * 2; //Reset to positive value (otherwise next thrust wouldn't work)
            }

            joint.anchor = originalJointAnchor - Vector2.right * thrustOffset;
        }
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        //Maybe if this is done on all clients we'll hide latency?

        lastSpearJumpTime = Time.time;
        float maxImpulse = thrusting ? maxImpulseThrusting : maxImpulseSwinging;
        float impulse = Mathf.Clamp(collision.contacts[0].relativeVelocity.magnitude * impulseMultiplier - ignoreImpulsesBelow, 0, maxImpulse);
        if (impulse > 1)
        {
            //Don't move self. Was a bit buggy on network.
            //vikingBody.AddForce(collision.contacts[0].relativeVelocity.normalized * impulse, ForceMode2D.Impulse);

            if(collision.otherRigidbody != null && collision.otherRigidbody.gameObject != gameObject && !collision.otherRigidbody.isKinematic)
                collision.otherRigidbody.AddForce(-collision.contacts[0].relativeVelocity.normalized * impulse, ForceMode2D.Impulse);
        }
    }
}
