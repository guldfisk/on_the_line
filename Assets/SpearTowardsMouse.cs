﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpearTowardsMouse : Photon.MonoBehaviour {

    public float lookAtTorqueLimit = 200;
    public float lookAtTorqueMultiplier = 20;
    public float damping = 3;

    Rigidbody2D body;
    SpringJoint2D joint;
    Transform tip;
    
	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
        joint = GetComponent<SpringJoint2D>();
        tip = transform.Find("Tip");
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (!photonView.isMine)
            return;

        Vector3 mousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float angleTowardsMouse = Vector2.SignedAngle(transform.right, mousePoint - joint.connectedBody.transform.position);
        float torque = angleTowardsMouse * lookAtTorqueMultiplier;

        //DAMPING
        torque -= body.angularVelocity * damping;

        //Prevent oscillation
        if(angleTowardsMouse < 15)
        {
            torque *= 0.4f;
        }

        body.AddTorque(Mathf.Clamp(torque, -lookAtTorqueLimit, lookAtTorqueLimit) * body.inertia);
    }
}
