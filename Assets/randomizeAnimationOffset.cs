﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomizeAnimationOffset : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var animator = GetComponent<Animator>();
        animator.ForceStateNormalizedTime(UnityEngine.Random.Range(0.0f, 1.0f));
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
